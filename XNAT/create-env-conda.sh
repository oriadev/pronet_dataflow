#!/bin/bash
# ## AUTHORS(s)
#
# * Zailyn Tamayo, Department of Psychiatry, Yale University
# * Lining Pan, Department of Psychiatry, Yale University
#
# ## COPYRIGHT NOTICE
#
# Copyright (C) 2021 Anticevic Lab, Yale University
#
# ## PRODUCT
#
# XNAT Upload Script
#
# * Script to upload files to XNAT
#
# ## LICENSE
#
# This Source Code is subject to the terms of the
# DataOrc License Agreement. You should have received a copy
# of the DataOrc License Agreement along with this program.
# A copy of the license is also available at <https://bitbucket.org/oriadev/pronet_dataflow/src/master/LICENSE.md>

# create a virtual environment in local folder
conda create -qy --prefix $(pwd)/env python=3.9

# install dependencies
$(pwd)/env/bin/pip install -r requirements.txt

# install xnat_upload
$(pwd)/env/bin/pip install .

# create bin directory that only exposes xnat-upload executable
mkdir bin

ln -s $(pwd)/env/bin/xnat-upload $(pwd)/bin/xnat-upload

echo "Add xnat-upload to path"
echo "export PATH=$(pwd)/bin:\${PATH}"
