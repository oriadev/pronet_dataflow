# XNAT Upload Tools 
## Create virtual environment
### conda (preferred method)
Install miniconda https://docs.conda.io/en/latest/miniconda.html

Create a virtual environment in current directory with name `env` and activate the virtual environment. 
```bash
conda create -qy --prefix $(pwd)/env python=3.9
source activate ./env
```
Always remember to activate the virtual environment before using `xnat-upload`. For more information on creating conda env, visit https://docs.conda.io/projects/conda/en/latest/commands/create.html

### python virtualenv
Required system packages: python3 >= 3.6, virtualenv

Debian:
```
sudo apt install python3 python3-venv
```
RHEL:
```
sudo yum install python3 virtualenv
```
```bash
# create virtual environment called `env` and activate
python3 -m venv env
source env/bin/activate
```
## Install dependencies and xnat-upload executable
```bash
# install dependencies
pip install -r requirements.txt

# install xnat_upload
pip install .
```

You may also install `xnat-upload` by executing `create-env.sh` (venv) or `create-env-conda.sh` (conda)
## Overview
`xnat-upload` should be invoked with two or more parts, including a main command and one or more sub-commands. The main command is responsible for handling XNAT server authentication, logging, and configuring the data directory.
```bash
 ---- main command
|---- xnat-upload --host=https://xnat.med.yale.edu \
|----         --datadir="/path/to/scan-dir" \
|----         --logdir="/path/to/log-dir" \

 ---- sub-commands
|---- 	upload-raw --project=test --xnatsub=subj1 --xnatlabel=pb000001 
|---- 	upload-raw --project=test --xnatsub=subj2 --xnatlabel=pb000002 
...
```

### Main command options: XNAT credentials
By default, the upload script will read XNAT credential stored at `~/.xnat`. You may override where the credentials are stored with option `--xnatcredentialfile`. You may reset you credential using option `--resetcredentials`. Sub-commands will not be evaluated if this option is specified. 
```bash
xnat-upload --resetcredentials
# Or provide path to xnat credential file
xnat-upload --xnatcredentialfile=/path/to/credential_file --resetcredentials
```

### Main command option: log location
You can change where upload log is stored using `--logdir` option. Upload log will be stored at `~/.XNATUploadLog` by default.

## Upload raw - sample commands

### Upload a single session
```bash
xnat-upload --host=https://xnat.med.yale.edu \
        --datadir="/path/to/scan-dir" \
        --logdir="/path/to/log-dir" \
	upload-raw --project=test --xnatsub=subj1 --xnatlabel=pb000001 
```

### Upload multiple sessions by chaining multiple `upload-raw` commands
```bash
xnat-upload --host=https://xnat.med.yale.edu \
        --datadir="/path/to/scan-dir" \
    upload-raw --project=test --xnatsub=subj1 --xnatlabel=pb000001 \
    upload-raw --project=test --xnatsub=subj2 --xnatlabel=pb000002
```
Session `pb000001` will be uploaded first. The script **waits** until XNAT finishes import and archive before uploading session `pb000002`.

## Upload resource - sample command
Currently only one file can be passed to `upload-resource` sub-command at a time. However, the file may be an archive (`*.zip`, `*.tar`, `*.tar.gz`) and XNAT server can automatically extract the archive if the `--extract` flag is turned on.


```bash
xnat-upload --host=https://xnat.med.yale.edu \
        --datadir="/path/to/resource-dir" \
	upload-resource --project=test \
        --xnatsub=subj1 --xnatlabel=pb000001 \
        --reslabel=EEG --extract \
        "EEG Data.zip"
```
The `upload-resource` command accepts path relative to `datadir` and absolute path as `file` argument, which may be useful if `upload-raw` and `upload-resource` are chained together.


