# ## AUTHORS(s)
#
# * Zailyn Tamayo, Department of Psychiatry, Yale University
# * Lining Pan, Department of Psychiatry, Yale University
#
# ## COPYRIGHT NOTICE
#
# Copyright (C) 2021 Anticevic Lab, Yale University
#
# ## PRODUCT
#
# XNAT Upload Script
#
# * Script to upload files to XNAT
#
# ## LICENSE
#
# This Source Code is subject to the terms of the
# DataOrc License Agreement. You should have received a copy
# of the DataOrc License Agreement along with this program.
# A copy of the license is also available at <https://bitbucket.org/oriadev/pronet_dataflow/src/master/LICENSE.md>

from .command import *

if __name__ == "__main__":
    upload()
