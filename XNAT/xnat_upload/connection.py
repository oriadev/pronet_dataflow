# ## AUTHORS(s)
#
# * Zailyn Tamayo, Department of Psychiatry, Yale University
# * Lining Pan, Department of Psychiatry, Yale University
#
# ## COPYRIGHT NOTICE
#
# Copyright (C) 2021 Anticevic Lab, Yale University
#
# ## PRODUCT
#
# XNAT Upload Script
#
# * Script to upload files to XNAT
#
# ## LICENSE
#
# This Source Code is subject to the terms of the
# DataOrc License Agreement. You should have received a copy
# of the DataOrc License Agreement along with this program.
# A copy of the license is also available at <https://bitbucket.org/oriadev/pronet_dataflow/src/master/LICENSE.md>

import logging
from time import sleep
import requests
from requests.exceptions import Timeout, ConnectionError
from requests_toolbelt import MultipartEncoder
from requests.compat import urljoin
from requests_toolbelt.adapters.socket_options import TCPKeepAliveAdapter


class XNATConnection():
    """Handles connection to XNAT server"""

    def __init__(self, server_addr, username, password):
        self.server_addr = server_addr
        self.username = username
        self.password = password
        self.session = requests.Session()
        keep_alive = TCPKeepAliveAdapter(idle=120, count=20, interval=30)
        self.session.mount('http://', keep_alive)
        self.session.mount('https://', keep_alive)
        self.connect()  # automatically connect

    def __del__(self):
        self.close()

    def connect(self):
        res = self._get_session_id()

    def close(self):
        """Delete session and close connection"""
        self._delete_session_id()
        self.session.close()

    def _get_url(self, api_path):
        """Get concatenated url with server address and path"""
        return urljoin(self.server_addr, api_path)

    def throw_http_error(func, error_list=None, handler=None):
        def inner(self, *args, **kwargs):
            res = func(self, *args, **kwargs)
            if res.status_code >= 400:
                raise Exception("Http error: {:d}".format(res.status_code))
            return res
        return inner

    def retry_on_auth_error(func):
        """Decorator automatically retry if session id is expired"""

        def inner(self, *args, **kwargs):
            res = func(self, *args, **kwargs)
            if res.status_code == requests.codes["unauthorized"]:
                self.connect()
                res = func(self, *args, **kwargs)
            return res
        return inner

    def _http_request(self, method, path, *args, **kwargs):
        """Retry on Timeout and ConnectionError, using exponential backoff loop
        """
        backoff = 1  # initial backoff 1s
        max_retry = 10
        retry = 0
        while True:
            try:
                res = self.session.request(method, path, *args, **kwargs)
                if res.status_code in [502, 503, 504]:
                    raise Timeout(f"Status code: {res.status_code}")
                return res
            except Timeout as e:
                logging.debug("Timeout occurred")
                logging.debug(e.strerror)
            except ConnectionError as e:
                logging.debug("Connection error occurred")
                logging.debug(e.strerror)
            if retry >= max_retry:
                raise Timeout(f"Reached max retry: {max_retry}")
            sleep(backoff)
            backoff *= 2
            retry += 1

    @retry_on_auth_error
    def http_get(self, path, *args, **kwargs):
        """Wrapper around requests get
        Server address will be automatically added
        Authentication is handled automatically
        """
        return self._http_request("GET", self._get_url(path), *args, **kwargs)

    @retry_on_auth_error
    def http_post(self, path, *args, **kwargs):
        """Wrapper around requests post
        Server address will be automatically added
        Authentication is handled automatically
        """
        return self._http_request("POST", self._get_url(path), *args, **kwargs)

    @retry_on_auth_error
    def http_delete(self, path, *args, **kwargs):
        """Wrapper around requests delete
        Server address will be automatically added
        Authentication is handled automatically
        """
        return self._http_request("DELETE", self._get_url(path), *args, **kwargs)

    @retry_on_auth_error
    def http_put(self, path, *args, **kwargs):
        """Wrapper around requests put
        Server address will be automatically added
        Authentication is handled automatically
        """
        return self._http_request("PUT", self._get_url(path), *args, **kwargs)

    def upload_file_multipart(
            self, file_path,
            remote_path, remote_file_name,
            mime_type="application/octet-stream", extract=False):
        """Upload a file with steaming

        The file stream will be consumed by `put`, need special retry logic"""
        backoff = 1  # initial backoff 1s
        max_retry = 10
        retry = 0
        while True:
            try:
                with open(file_path, "rb") as f:
                    form_key = {'file': (remote_file_name, f, mime_type)}
                    m = MultipartEncoder(fields=form_key)
                    # multipart encoder consumes file stream, unable to retry, force refresh session id before upload.
                    params = {}
                    if extract:
                        params["extract"] = "true"
                    self.connect()
                    res = self.session.put(
                        self._get_url(remote_path),
                        data=m,
                        headers={'Content-Type': m.content_type},
                        params=params)
                if res.status_code in [502, 503, 504]:
                    raise Timeout(f"Status code: {res.status_code}")
                return res
            except Timeout as e:
                logging.debug("Timeout occurred")
                logging.debug(e.strerror)
            except ConnectionError as e:
                logging.debug("Connection error occurred")
                logging.debug(e.strerror)
            if retry >= max_retry:
                raise Timeout(f"Reached max retry: {max_retry}")
            sleep(backoff)
            backoff *= 2
            retry += 1

    @throw_http_error
    def _get_session_id(self):
        req_url = self._get_url("/data/JSESSION")
        return self.session.post(req_url, auth=(self.username, self.password))

    def _delete_session_id(self):
        if "JSESSIONID" in self.session.cookies:
            req_url = self._get_url("/data/JSESSION")
            return self.session.delete(req_url)
