# ## AUTHORS(s)
#
# * Zailyn Tamayo, Department of Psychiatry, Yale University
# * Lining Pan, Department of Psychiatry, Yale University
#
# ## COPYRIGHT NOTICE
#
# Copyright (C) 2021 Anticevic Lab, Yale University
#
# ## PRODUCT
#
# XNAT Upload Script
#
# * Script to upload files to XNAT
#
# ## LICENSE
#
# This Source Code is subject to the terms of the
# DataOrc License Agreement. You should have received a copy
# of the DataOrc License Agreement along with this program.
# A copy of the license is also available at <https://bitbucket.org/oriadev/pronet_dataflow/src/master/LICENSE.md>

import os
import sys
import logging
import time
import pathlib
import click
import datetime
import json


def prepare_log(ctx):
    """Prepare python logger

    Create log file at debug level
    Stream a copy of log at debug or info level to console

    Input: Click context
    Output: None
    """
    logdir = ctx.obj['LOGDIR']
    if not os.path.exists(logdir):
        try:
            os.mkdir(logdir)
        except OSError as e:
            # log file not ready
            click.echo("Error creating log folder", err=True)
            click.echo(str(e), err=True)
            exit(e.errno)
    ctx.obj['LOG_FILE'] = os.path.join(logdir, "xnat_upload_{}.log".format(ctx.obj['TIMESTAMP']))
    logging.basicConfig(
        format='%(asctime)s:%(levelname)s:%(message)s',
        filename=ctx.obj['LOG_FILE'],
        # encoding='utf-8',
        datefmt='%Y/%m/%d %H:%M:%S',
        level=logging.DEBUG)
    stream_handler = logging.StreamHandler(sys.stdout)
    if ctx.obj['DEBUG']:
        stream_handler.setLevel(logging.DEBUG)
    else:
        stream_handler.setLevel(logging.INFO)
    logging.getLogger().addHandler(stream_handler)
    logging.info("Upload log located at: {:s}".format(ctx.obj['LOG_FILE']))

def get_current_timestamp():
    return datetime.datetime.now().strftime("%Y%m%d%H%M%S")

def validate_session_folder(datadir, xnatsub, xnatlabel):
    session_dir = os.path.join(datadir, xnatsub, xnatlabel)
    if os.path.exists(session_dir):
        return session_dir
    return None

def find_file_with_pattern(folder, patterns):
    """Recursively find all files in folder that match any patterns in the list
    Input:
        folder: path string
        patterns: a list of patterns to match (e.g. ["*.dcm*", "*.dicom.zip*", "MR*", "*.IMA*"])
    """
    file_list = set()
    for (path, dirs, files) in os.walk(folder):
        fs = set()
        for p in patterns:
            fs |= set(pathlib.Path(path).glob(p))
        for f in fs:
            if os.path.isfile(f):
                file_list.add(f)
    return sorted(file_list)



def monitor_event(server, event_tracking_id, timeout=3600, interval=10):
    """ Monitor event through the event tracking api
    Wait until the event finishes or timeout
    timeout in seconds (default 1h, 0 for no timeout)
    """
    assert timeout >= 0
    start_time = datetime.datetime.now()
    while True:
        if timeout != 0 and start_time + datetime.timedelta(seconds=timeout) < datetime.datetime.now():
            return (False, "Timeout")
        logging.info("Event in progress")
        res = server.http_get(
            "/xapi/event_tracking/{}".format(event_tracking_id),
            headers={'Accept': 'application/json'})
        #event tracking api may not be available immediately
        if res.status_code == 404:
            logging.info("Waiting for event tag")
            time.sleep(1)
            continue
        logging.debug(res.text)
        event_status = json.loads(res.text)
        if event_status["succeeded"] is not None:
            return(event_status["succeeded"], event_status["finalMessage"])
        time.sleep(interval)

def get_script_version():
    from .version import VERSION
    return VERSION
