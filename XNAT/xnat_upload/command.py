# ## AUTHORS(s)
#
# * Zailyn Tamayo, Department of Psychiatry, Yale University
# * Lining Pan, Department of Psychiatry, Yale University
#
# ## COPYRIGHT NOTICE
#
# Copyright (C) 2021 Anticevic Lab, Yale University
#
# ## PRODUCT
#
# XNAT Upload Script
#
# * Script to upload files to XNAT
#
# ## LICENSE
#
# This Source Code is subject to the terms of the
# DataOrc License Agreement. You should have received a copy
# of the DataOrc License Agreement along with this program.
# A copy of the license is also available at <https://bitbucket.org/oriadev/pronet_dataflow/src/master/LICENSE.md>

import click
import os
import sys
import logging
import zipfile
import requests

from .connection import XNATConnection
from .utils import get_script_version, monitor_event, prepare_log, \
    get_current_timestamp, validate_session_folder, \
    find_file_with_pattern


def resetCredential(ctx, param, value):
    if not value:
        return
    # ctx.lookup_default("logdir")
    # if "host" in ctx.params:
    #     host = ctx.params['host']
    # else:
    #     host = ctx.lookup_default('host')
    #     if host is None:
    #         click.echo("Must provide --host option before resetcredentials")
    #         ctx.exit()

    if "xnatcredentialfile" in ctx.params:
        cred_file = ctx.params['xnatcredentialfile']
    else:
        cred_file = ctx.lookup_default('xnatcredentialfile')
        if cred_file is None:
            click.echo(
                "Must provide --xnatcredentialfile option before resetcredentials")
            ctx.exit()

    if os.path.exists(cred_file):
        if click.confirm(f"Delete XNAT credential file: {cred_file}"):
            os.remove(cred_file)
            click.echo("Credential file removed")
    else:
        click.echo(f"Credential file: {cred_file} does not exist")
    ctx.exit()


CONTEXT_SETTINGS = dict(
    default_map={
        'xnatcredentialfile': os.path.expanduser("~/.xnat"),
        'resetcredentials': False,
        'logdir': os.path.expanduser("~/.XNATUploadLog"),
        'debug': False
    }
)


@click.group(chain=True, context_settings=CONTEXT_SETTINGS)
@click.option('--host', required=True)
@click.option('--datadir', required=True, type=click.Path(exists=True, file_okay=False, dir_okay=True))
@click.option('--xnatcredentialfile', type=click.Path(file_okay=True, dir_okay=False))
@click.option('--resetcredentials', is_flag=True, callback=resetCredential, expose_value=False)
@click.option('--logdir', type=click.Path(file_okay=False, dir_okay=True))
@click.option('--debug', is_flag=True, help="print debug message to stdout")
@click.version_option(version=get_script_version())
# @click.option('--dry-run', default=False, is_flag=True)
@click.pass_context
def upload(ctx, host, datadir, xnatcredentialfile, logdir, debug):
    """Upload to raw scan or resource to XNAT

    This tool allow you to chain multiple upload sub-commands together.
    Sub-commands in a chain is executed one-by-one in series. Note that
    the upload-raw command will wait until XNAT completes import.
    """
    ctx.ensure_object(dict)
    ctx.obj['TIMESTAMP'] = get_current_timestamp()
    ctx.obj['LOGDIR'] = logdir
    ctx.obj['CRED_FILE'] = xnatcredentialfile
    ctx.obj['DEBUG'] = debug
    # ctx.obj['DRY_RUN'] = dry_run
    ctx.obj['DATADIR'] = datadir

    prepare_log(ctx)
    logging.debug("Xnat upload script version: {:s}".format(
        get_script_version()))
    logging.debug("Executing command: {:s}".format(" ".join(sys.argv)))

    # get username and password
    # if reset or credential file does not exist
    prompt_credential = True
    if not os.path.exists(ctx.obj['CRED_FILE']):
        prompt_credential = True
        logging.warn("XNAT credentials in {:s} NOT found.".format(
            ctx.obj['CRED_FILE']))
    else:
        # load credential from file
        try:
            with open(ctx.obj['CRED_FILE']) as f:
                logging.info("XNAT credentials in {:s} found.".format(
                    ctx.obj['CRED_FILE']))
                username, password = f.readline().strip().split(":", 1)
                ctx.obj['USERNAME'] = username
                ctx.obj['PASSWORD'] = password
                prompt_credential = False
        except OSError as e:
            logging.warning(
                'Unable to read credential file: {:s}'.format(ctx.obj['CRED_FILE']))

    # prompt for username and password
    if prompt_credential:
        click.echo(
            "Please enter your credential for XNAT site: {:s}".format(host))
        ctx.obj['USERNAME'] = click.prompt("Username", type=str)
        ctx.obj['PASSWORD'] = click.prompt(
            "Password", type=str, hide_input=True)

    # save credential with read-only by owner permission
    if os.path.exists(ctx.obj['CRED_FILE']):
        os.remove(ctx.obj['CRED_FILE'])
    with open(ctx.obj['CRED_FILE'], "w") as f:
        f.write("{:s}:{:s}".format(ctx.obj['USERNAME'], ctx.obj['PASSWORD']))
    os.chmod(ctx.obj['CRED_FILE'], 0o400)
    logging.info('XNAT credentials generated in {:s}'.format(
        ctx.obj['CRED_FILE']))

    # create xnat connection
    ctx.obj['XNAT_SERVER'] = XNATConnection(
        host, ctx.obj['USERNAME'], ctx.obj['PASSWORD'])


@upload.command()
@click.pass_context
@click.option('--project', required=True, help="XNAT project id/label")
@click.option('--xnatsub', required=True, help="XNAT subject id/label")
@click.option('--xnatlabel', required=True, help="XNAT experiment label")
def upload_raw(ctx, project, xnatsub, xnatlabel):
    """Upload raw dicom to XNAT server"""

    # Validate session folder
    logging.info("Checking folder: datadir: {:s}, subject: {:s}, label: {:s}".format(
        ctx.obj["DATADIR"], xnatsub, xnatlabel))
    session_dir = validate_session_folder(
        ctx.obj["DATADIR"], xnatsub, xnatlabel)
    if session_dir is None:
        logging.warn("Data folder does not exist for subject: {:s} label: {:s}".format(
            xnatsub, xnatlabel))
        logging.warn("Skip upload for subject: {:s} label: {:s}".format(
            xnatsub, xnatlabel))
        return

    # Prepare tmp folder
    zip_dir = os.path.join(ctx.obj["DATADIR"], "xnattemp")
    try:
        if not os.path.exists(zip_dir):
            os.mkdir(zip_dir)
    except Exception:
        logging.error("Unable to create tmp folder")
        return

    # Find all dicom files in session folder
    logging.info("Find file list to upload")
    file_list = find_file_with_pattern(
        session_dir, ["*.dcm*", "*.dicom.zip*", "MR*", "*.IMA*"])
    logging.info("Found {:d} files in the data folder".format(len(file_list)))

    # Create zip file
    sub_zip_file = os.path.join(zip_dir, "{:}.zip".format(xnatsub))
    if os.path.exists(sub_zip_file):
        logging.warn(
            "Zip file already exists {:}. Will be removed".format(sub_zip_file))
        os.remove(sub_zip_file)

    logging.info("Creating zip file {:}".format(sub_zip_file))
    with zipfile.ZipFile(sub_zip_file, 'w', compression=zipfile.ZIP_DEFLATED) as z:
        for f in sorted(file_list):
            z.write(f, arcname=os.path.relpath(f, session_dir))
    logging.info("Zip file {:} created".format(sub_zip_file))

    # Upload to User Cache
    logging.info("Uploading to user cache")
    res = ctx.obj["XNAT_SERVER"].upload_file_multipart(
        file_path=sub_zip_file,
        remote_path="/data/user/cache/resources/{0:}/files/{0:}.zip".format(
            xnatsub),
        remote_file_name="{0:}.zip".format(xnatsub),
        mime_type="application/octet-stream")
    if res.status_code != requests.codes["ok"]:
        logging.warn("Upload unsuccessful")
        return

    # Import from cache
    logging.info("Import from cache")
    event_id = "{ts}_{subj}_{label}".format(
        ts=get_current_timestamp(), subj=xnatsub, label=xnatlabel)
    logging.debug("Event tracking id: {}".format(event_id))
    res = ctx.obj["XNAT_SERVER"].http_post(
        "/data/services/import",
        params={"src": "/user/cache/resources/{0:s}/files/{0:s}.zip".format(xnatsub),
                "dest": "/archive/projects/{0:}/subjects/{1:}/experiments/{2:}".format(project, xnatsub, xnatlabel),
                "http-session-listener": event_id},
        headers={'Accept': 'application/json'})
    if res.status_code != requests.codes["ok"]:
        logging.warn("Import request failed")
        return

    # Monitor import
    # timeout = 4 * 3600 # 4 hours
    timeout = 0
    interval = 120
    logging.info("Monitor import with timeout: {:}s, event interval: {:}s".format(
        timeout, interval))
    (succeeded, final_message) = monitor_event(
        server=ctx.obj["XNAT_SERVER"],
        event_tracking_id=event_id,
        timeout=timeout,
        interval=interval)
    if succeeded:
        logging.info("Import succeeded")
    else:
        logging.info("Import failed")
    logging.info("Final message: {}".format(final_message))

    # remove zip from tmp
    logging.info("Remove temporary zip file: {:s}".format(sub_zip_file))
    if os.path.exists(sub_zip_file):
        os.remove(sub_zip_file)


@upload.command()
@click.option('--project', required=True, help="XNAT project id/label")
@click.option('--xnatsub', required=True, help="XNAT subject id/label")
@click.option('--xnatlabel', required=True, help="XNAT experiment label")
@click.option('--reslabel', required=True, help="resource folder name")
@click.option('--extract', default=False, is_flag=True, help="Ask XNAT server to extract uploaded archive")
@click.argument('file')
@click.pass_context
def upload_resource(ctx, project, xnatsub, xnatlabel, reslabel, extract, file):
    """Upload resource

    File can be specified using absolute path or path relative to datadir

    Only experiment level resource can be uploaded. Only one input file is supported.
    However, the file may be an archive (*.zip, *.tar, *.tar.gz) and XNAT server can
    automatically extract the archive if the --extract flag is turned on.
    """

    logging.info("Upload resource")
    if not os.path.isabs(file):
        file = os.path.join(ctx.obj["DATADIR"], file)
    if not os.path.exists(file):
        logging.warn("Resource file does not exist: {:s}".format(file))
        return

    file_name = os.path.basename(file)
    remote_path = "/data/projects/{project}/subjects/{xnatsub}/experiments/{xnatlabel}/resources/{reslabel}/files/{file_name}".format(
        project=project,
        xnatsub=xnatsub,
        xnatlabel=xnatlabel,
        reslabel=reslabel,
        file_name=file_name)
    res = ctx.obj["XNAT_SERVER"].upload_file_multipart(
        file_path=file,
        remote_path=remote_path,
        remote_file_name=file_name,
        extract=extract
    )
    if res.status_code == 200:
        logging.info("Upload successful")
    else:
        logging.warn("Upload failed")
