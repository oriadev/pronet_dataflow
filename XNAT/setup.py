# ## AUTHORS(s)
#
# * Zailyn Tamayo, Department of Psychiatry, Yale University
# * Lining Pan, Department of Psychiatry, Yale University
#
# ## COPYRIGHT NOTICE
#
# Copyright (C) 2021 Anticevic Lab, Yale University
#
# ## PRODUCT
#
# XNAT Upload Script
#
# * Script to upload files to XNAT
#
# ## LICENSE
#
# This Source Code is subject to the terms of the
# DataOrc License Agreement. You should have received a copy
# of the DataOrc License Agreement along with this program.
# A copy of the license is also available at <https://bitbucket.org/oriadev/pronet_dataflow/src/master/LICENSE.md>

from setuptools import setup
from xnat_upload.version import VERSION
setup(
    name="xnat_upload",
    version=VERSION,
    packages=["xnat_upload"],
    package_dir={'xnat_upload': 'xnat_upload'},
    install_requires=["click", "requests", "requests-toolbelt"],
    entry_points={
            'console_scripts': [
                'xnat-upload=xnat_upload.command:upload',
            ]
    }
)
